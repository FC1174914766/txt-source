蘇菲亞孤兒院，就在離治療院不遠之處。

其外觀是一間普通的獨棟民房，建築物已有一定的老舊程度。

孤兒院由一位看來為人和善，名叫蘇菲亞的老婆婆獨自經營，所扶養的孤兒人數有一位嬰兒，以及五歳至十二歳左右的八位男孩及女孩，規模非常非常小。

我們到達這間孤兒院，是十幾分鐘前的事。

我和愛麗絲一起向院長蘇菲亞女士打過招呼後，就立刻開始分擔孤兒院的工作了。

哎，雖說是工作，但其實很簡單啦。

蘇菲亞女士吃完午餐後要去與人會面，這段期間她希望我們幫忙照顧孩子們，就這樣而已。

愛麗絲似乎也打算去幫忙做飯，我們來得好像稍微早了些。

現在愛麗絲正背著嬰兒並製作蘇菲亞女士與孩子們的午餐⋯⋯

「哥哥好厲害──！超厲害一─！」
「再一次、再來一次！」
「欸，這要怎麼弄呀？是怎麼弄出來的？哥哥快教教我！」
「請、請您教我吧！我也想試試看，主人！」

我則在這段期間避免讓這些淘氣十足的孩子們做出調皮搗蛋的事，而設法使他們安分地坐在這間餐廳裡。

孩子們羨慕與尊敬的目光投射在我身上，若要具體地說明我做了什麼，那就是──摺紙。

「如何？這次摺的，是一種叫做鶴的鳥。」
「好強──！」
「再一次！再一次！」
「欸，這是怎麼摺的？一、一開始摺成三角形就可以了嗎？」
「主人您好厲害、好厲害哦！」

孩子們大概是覺得紙隨著摺疊而漸漸形成已知動物的形狀很新鮮，每一個人都入神地盯著我的手不放。

看來這個国家還沒有摺紙來玩的文化。

附帶一提，我之所以擅於摺紙，是由於小學時所做過的※自由研究就是摺紙。（譯註：日本中小學生的暑假作業之一，要做在學校沒有做過的研究，項目自行決定。）

雖說幾乎是把整本摺紙教學抄過來的，根本不算是什麼研究，但其中的內容我到現在還大致記得。

摺紙是有一定規則存在的，只要或多或少地記住這些規則，也能夠即興摺出些簡單的東西。

鶴這種基本款自是不用提，像動物的臉這種比較簡單的，我靠即興摺法就能輕鬆摺出來。

沒想到當時的經驗居然會在這種時候派上用場。

老實說，當愛麗絲交待我「在我做飯時，幫忙看著孩子們，不要讓他們做出危險的事」時，還在想說該怎麼辦，結果實際幫忙帶孩子後，其實也沒什麼難的嘛。

我就這樣實地示範摺紙，讓孩子們產生興趣後⋯⋯

「那這些紙就給你們，你們就自己試著摺出喜歡的東西吧。」
「我、我要摺哥布林！」
「我想摺史萊姆。」
「欸，小兔兔要怎麼摺？欸，教我嘛？」
「我想要摺出主人。」

再把紙分出去，隨興地看他們摺就好。

若孩子們到處亂跑亂跳可就麻煩了，只要像這樣給他們新玩具，之後就都在我的掌握裡了。

孩子們一個個地乖乖地專心摺起紙來。

只要偶爾給一些建議，再看時機摺出一個新的範本讓他們見識，應該就足夠消磨愛麗絲她們做完飯之前的時間了吧。

哎，只要交給我，照顧小孩小事一樁啦。

說不定這樣就能讓愛麗絲從我身上感受到父性，進而使她產生「和他一起生育小孩或許也不錯⋯⋯」的想法了呢。

──就在這麼思考的時候，我看到混在孩子堆裡的悠艾兒，她的手突然停了下來。

⋯⋯這麼說來，悠艾兒小姐不知何時之間混進了孤兒裡頭，由於實在過於自然，以至於我沒有表達任何意見。

她會不會是無法順利摺出主人，想要向我尋求建議呢？

不過，這就算是我也沒有法子。

品質高到能夠看出特定人物的摺紙作品，我可摺不出來。

希望她起碼只要求我摺個可愛的動物臉孔就好。

⋯⋯然而，悠艾兒的手是停了下來，但她並沒有看我。

在悠艾兒的視線前方，有一位年紀大約七歳的幼小女孩。

⋯⋯那孩子怎麼了嗎？

在周圍的孩子們都熱衷於摺紙時，悠艾兒就只是注視著那位小女孩。

⋯⋯看著那位女孩，才讓我發覺到一件事。

她手上沒有拿著紙，甚至沒有在看紙。

應該說，她看起來像是對摺紙沒有興趣。

不過她正在摩擦著握拳的手，那只手裡似乎握著什麼。

而連這樣的動作，感覺都有些心不在焉。

她就只是呆呆地坐著，像是在等待時間流逝。

「你怎麼了？覺得摺紙很無聊嗎？」
「唔⋯⋯」

我試著若無其事地向她說起話來，結果小女孩的雙眼浮出眼淚，把頭低了下去。

⋯⋯大概是怕生吧。

也對，當有不認識的人突然出現時，也會有小孩表現出這種反應。

「沒關係，紙還有很多，如果你想摺了，之後再試試看吧。」

我姑且又說了一句話後──

「唔⋯⋯嗚⋯⋯嗚嗚⋯⋯」

小女孩就把臉埋進膝蓋裡，哭了出來。

「喂，別、別哭啦！我、我什麼都沒做吧？」

該、該怎麼辦才好啊？沒想到只是說句話就把她弄哭了。

要怎麼應付這樣的小孩，我就不曉得了。

再說她也許只是單純害怕成人男性。

對了，去叫愛麗絲，請她來幫忙吧。

──而正當我要去叫愛麗絲時，看到小女孩這般反應的悠艾兒不知為何朝著我看。怎麼回事？她的眼神像是在期待著什麼。

⋯⋯她在期待什麼？

期待我現在立即華麗地讓這位小女孩破涕為笑之類的嗎？

⋯⋯好像不是這樣。

我有點搞不清楚悠艾兒的想法。

正當我覺得疑惑時⋯⋯

「主人，這孩子的眼睛可能看不見。」

悠艾兒開口如此說道，讓小女孩的肩膀震了一下

「⋯⋯真的嗎？」

看起來不像啊。

⋯⋯不對，曾經有過一段盲眼時期的悠艾兒說不定能夠看得出來。

小女孩微微地抬起臉，看向悠艾兒。

她露出了覺得奇怪的表情，像是在說「你怎麼知道的？」

⋯⋯仔細一瞧，她的瞳孔焦點，或者說是視線，感覺的確有點怪怪的。

⋯⋯原來如此。

她是因為眼睛看不見，才無法摺紙的，是這麼一回事啊。

「⋯⋯不，其實我看得見一點點。可是⋯⋯這種纖細的事就做不到了，因為我只看得到一片模糊。」

小女孩以顫抖的聲音向悠艾兒說道。

⋯⋯就連自己手上的東西都模糊得看不清楚。

我曾經聽過這種事。

這孩子大概是弱視吧。

雖然能隱約看見模糊的四周景象，但對於細節就無法對上焦點，或是看到的物體像是蒙上了一層霧──她可能就處於這樣的狀況。

「不過，是眼睛啊⋯⋯眼睛就⋯⋯」

⋯⋯有視力障礙應該會很不方便吧。

要治療眼睛，得用到究極療癒才行。對於這位只是一介孤兒的小女孩，或許一生都無法獲得治療。

在孤兒院還有能力照顧她時是還好，但之後可就束手無策了。

若是眼睛看不到，要找工作也會很困難。

目不能見物的女孩在這個世界會有什麼下場，大概能夠想像得到。

⋯⋯不是當奴隷，就是到妓院去，八成就是這樣吧。

這間孤兒院的老婆婆蘇菲亞女士在這孩子長大之後可能也會繼續照顧她，但她的歳數也很大了，不可能一直活在這世上。

這位年幼的女孩，在不久的將來想必會吃盡苦頭。

⋯⋯就在我想著這些事情時，悠艾兒一直盯著我看。

她的眼神果然像是在期待著什麼。

⋯⋯我以前曾經治癒過悠艾兒的眼睛。

悠艾兒的眼神讓人一看就曉得，她深信自己溫柔善良的主人不可能會對這位失明的孩子見死不救，也期待著見識到主人的帥氣表現。

就在悠艾兒以這樣的目光注視著我時，小女孩低著頭開口說道：

「在祭典舉行的時候，我聽說聖女大人一定能治好我的眼睛，但她只治療到排我前面的人就結束了⋯⋯結果還是不行。」
「啊──是那時候的事啊。」

這麼說來，從祭典的前幾天開始，聖女提供過免費的治療。

這位小女孩本來期待著聖女治好她的眼睛嗎？

不過，從聖女無法治癒所有被地龍噴到石化吐息的人這點來看，她就只能使用幾次究極療癒而已。

大概是剛好在排到這孩子時就用盡了魔力吧。

「不過沒關係，聖女大人她告訴我『一定會有好事發生，不要放棄』還給了我這個。」

小女孩說完後，就展示出自己手掌裡的東西。

「這是⋯⋯戒指嗎？」

第一眼看來，是個滿高貴的戒指。

上頭不但用了金子，還鑲有大顆的寶石，裝飾也很纖細。

在這世界若要做出這種品質的戒指，想必要花上不少錢。

若拿去賣掉，應該可以賣個好價錢。

⋯⋯應該不是什麼奇怪的魔道具吧，就像露露卡給我的耳環一樣。

我一聽到這戒指是聖女給她的，不知為何就戒備了起來。

再加上聖女的說法，彷佛是拿著就會有好事發生似的。

總之我先鑑定看看。

戒指。

⋯⋯看來就只是個單純的戒指。

不是魔道具也不是其他東西，而是個毫無怪異之處的高級金戒指。

也就是說，聖女就只是在施捨而已嗎？她的意思也許是說，要是真的走到窮途末路，就將這個戒指賣掉吧。

「聽說聖女大人還在這個城鎮裡，所以只要拿著這個，她一定就會來治療我了。」

接著小女孩說了這樣的話，並撫摸著戒指。

⋯⋯她應該還不知道聖女已經離開城鎮了吧。

要是這個小女孩之後知道這件事，她會露出怎樣的表情呢？

我將視線自小女孩身上移開後，發現悠艾兒還在看著我。

她以閃閃發光的眼神看著我。

⋯⋯真沒辦法。

我無法背叛悠艾兒這樣的目光。

要是現在對這小女孩見死不救，我將不再是悠艾兒的尊敬對象。

⋯⋯幸好聖女已經從城鎮裡離開了。

雖然還不致於可以毫無後顧之憂地施展治癒魔法，但應該也不到眼前出現需要治療的對象時，還非得隱瞞不可的地步。

再說，這孩子本來就能夠看得到一點點。

只要好好交待她要保密，應該能夠瞞得過一陣子。

然後隨著時間的經過，若這小女孩沒有主動說出治療的人是我，大概就不會有人得知是誰治好她的了。

「你叫什麼名字？」
「我⋯⋯？我叫，法菈。」
「這樣啊，法菈⋯⋯我有話要和你說，可以跟我過來一下嗎？」

也罷，就期待她將來會成長為愛麗絲那樣的美女來向我報恩吧。

我將其他孩子交給悠艾兒應付，並決定要治好這女孩。