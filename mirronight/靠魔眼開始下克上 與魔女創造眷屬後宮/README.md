# novel

- title: 魔眼で始める下剋上 魔女とつくる眷属ハーレム 1
- title_zh: 利用魔眼開始的下剋上 與魔女一同建立眷屬后宮 1
- author: 箕崎准
- illust: ゆか
- 翻譯: 涟漪
- 校對: 涟漪
- 圖源: 涟漪
- 漢化組:  矢雀汉化组
- source: https://www.lightnovel.us/detail/1077995
- cover: https://imgur.com/6Z2ZFer.jpg
- publisher: 
- date: 2021-06-01T20:00:00+08:00
- status: 連載中
- novel_status: 

## illusts

- ゆか

## publishers

- 角川 Sneaker文庫

## series

- name:  魔眼で始める下剋上 魔女とつくる眷属ハーレム 1

## preface


```
    與沉睡在地下城的魔女 ヴィノス 締下契約，入手《魔眼》的奴隸 エイト 和蘭古巴德王國的公主騎士 リリシア。當兩人回到地面時，王國居然遭受到古魯希亞帝國的強烈攻擊並變成一片火海!! 看著崩潰哭泣的 リリシア 的模樣，エイト 面帶微笑提出了一個建議「和我締結《眷屬契約》的話就能向帝國復仇」。リリシア 雖然對於和奴隸進行接吻這一屈辱般的行為感到憤怒且牢記在心，但作為公主的職責為了拯救心愛的國民才做此決定。散發出無力與悔恨的 リリシア，逐漸變成神魂飄散的表情「已經無法再忍耐了」並且將 エイト 給推倒……。

    僅供個人學習交流使用，禁作商業用途
    下載後請在24小時內刪除，LK、TSDM、demonovel不負擔任何責任
    請尊重翻譯、掃圖、錄入、校對的辛勤勞動，轉載請保留資訊
```

## tags

- node-novel
- R18
- 角川
- fantasy

# contribute

- 涟漪

# options

## downloadOptions

- noFilePadend: true
- filePrefixMode: 1
- startIndex: 1


## textlayout

- allow_lf2: false

# link
- [魔眼で始める下剋上 魔女とつくる眷属ハーレム](https://sneakerbunko.jp/series/magan/)
- [箕崎准@MisakiJun](https://twitter.com/MisakiJun)
- [PV](https://youtu.be/RjZ5q7Vyf1Q)