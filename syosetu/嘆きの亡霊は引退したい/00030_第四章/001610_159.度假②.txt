於是，祭典開始了。
我喜歡祭典。盧克和莉茲也非常喜歡快樂的事。西特莉也不討厭這些，安塞姆是個好哥哥所以會把妹妹們放在首位，因此我們從很久以前就習慣了吵吵鬧鬧。
然後，還總會被露西亞訓斥一頓。

雖然以前只是小小的活動，但隨著能用的金錢和權力的增加，結果就發展成了現在這般龐大的規模。儘管尋寶獵人多為追求一時歡娛之人，我也從未見到有誰像我們的隊伍這樣大規模舉辦過活動。

唯一存在的入口被土牆封閉，寬闊的空間裡瀰漫著濃濃的蒸汽。近處的桌子上擺放著大量的料理和酒，與溫泉的氣味融合構成了一個混沌的空間。
腳下雖然是泥土，卻被加固得像陶器那般擁有光澤，因此既不會滲水，也不會弄髒腳掌。

「唔哦～～～～溫泉啊～～～～～～～～～～～～！！克萊伊，給我也來頭龍！」

盧克用大到擾民的聲音發出咆哮，並盛大地跳進了溫泉之中。巨大的水花隨即濺起並朝著料理襲去，不過在千鈞一髮之際被看不見的牆壁彈開了。露西亞救得好。

看到這一幕，莉茲撅起嘴唇，並毫不猶豫地解開衣帶讓浴衣滑落。在燃燒的火光之中，晒得黝黑的肌膚閃耀著艷麗的光輝。

⋯⋯看來她並沒有穿內衣。對於他人的視線不以為意，還真是無所顧忌的脫衣舉動。雖然在加雷斯特山脈顯得羞澀，但現在似乎沒有不好意思。

寶具被解除，平時被包裹著的雙腿修長挺直，看上去略微有些性感。戴在腳踝上且處於待機狀態的至天之起源則是恰到好處地起著裝飾作用。

「好狡猾！明明我打算第一個進入的！」

「姐、姐姐大人！？怎麼這樣⋯⋯在Ｍ、Master的面前，太、太不檢點了！」

反倒是緹諾滿臉通紅地對莉茲說教起來。對於勇氣可嘉的弟子，莉茲眉毛倒豎地怒吼道。

「煩死了，小緹！事到如今，被看到也沒什麼好為難的！更何況也不是被看到身體就會為難的關係！你要是不想泡溫泉那就隨你便如何！？」

「怎麼這樣──」

「莉茲」

「！！Master！」

宛如得到天助似地，緹諾的臉上瞬間露出了笑容。
我把柔軟的海綿扔給一臉疑惑，並且和盧克一樣一絲不掛的莉茲然後說道。

「必須洗過身體才能進去」

「好～！不愧是克萊伊醬，真是通情達理！」

「Master！？」

看到眼睛閃閃發亮的莉茲，緹諾發出了悲鳴般的聲音。但是不好意思，正如莉茲所說現在也沒什麼好為難的。

我和莉茲的交情很深，最關鍵的是莉茲並不是靦腆之人。
雖然這次沒有做，但她其實是那種，不管是不是混浴都會若無其事地闖入男浴池的女生。當然不想闖入的時候是不會闖入的，但是只要想闖入她就會採取行動。然後，她還會用自豪的速度把其他礙事的客人打倒並扔到浴室外面。

而且她平時就穿得相當暴露，一起泡溫泉也並不是第一次，嘛～，簡單來說對於她的全裸⋯⋯我已經習慣了一些。只要不被黏著就不要緊。
除此之外，雖然以她的性格來說並不會因為赤身裸體就對肢體接觸產生顧慮，但是露西亞在的時候會用魔法將她擊退，所以完全沒有問題。而且露西亞甚至會施加視覺上的濾鏡。

雖然露西亞也深深地嘆了口氣，但也僅限於此。緹諾則像是因為感受到文化差異而深受衝擊的樣子，不過這在準備溫泉的時候就已經可以預見。

「今天就⋯⋯不拘禮節。盡情地吃喝吧」

「小緹，雖然是理所當然的事⋯⋯⋯⋯但不用脫也沒有問題的」

「可以圍上浴巾，也可以穿著泳衣哦。不脫衣服當然也沒有問題」

雖然既不穿泳衣也沒有圍浴巾，甚至不怕被他人看光的約有兩頭，但是完全不用在意。他們就像是野獸一樣的存在。緹諾真的不用在意。

「看啊！克萊伊！就算在大漩渦裡也沒有問題！這是修行的成果！而且還可以鍛鍊腰腿！」

盧克一邊在大漩渦裡踩水一邊自信滿滿地喊道。
看到他那樣我也覺得有些開心，一切也都感到無所謂起來，然後由於蒸汽太悶熱我就解開了衣服的紐扣。
總之先脫上半身吧。我逐個摘掉寶具的戒指，只留下一個結界指。同伴在身邊就是不錯，平日連睡覺時都戴著的寶具基本都能取下。

「Ｍ、Master！？Master也⋯⋯那個，要脫嗎？」

事到如今還說什麼啊⋯⋯我可是最無所謂的那個喲。只是脫點衣服誰都不會在意的吧。
說到底，緹諾不是也破壞過牆壁然後進入男浴室的嗎⋯⋯雖然我不會舊事重提。

仍舊穿著浴衣的露西亞啪地打響手指，酒杯便自行倒滿了酒，接著她把酒杯遞到了我的手邊。這是低度數的，我也能喝的甜酒。
我心懷感激地接下，然後眯起眼睛舉起了酒杯。

「飲酒後馬上泡澡對身體不好喲」

「⋯⋯⋯⋯那你為什麼讓我們準備啊」

不是我讓你們準備的。是莉茲她們擅自拿來的。我們，《嘆息的亡靈》就是如此地隨性。
算啦，今天不拘禮節。我就不多說些什麼了。

我把嘴對上酒杯，然後一口喝下了露西亞冰鎮過的酒。
緹諾咽了一口唾沫，接著像是下定了決心似地說道。

「Ｍ、Master⋯⋯⋯⋯我⋯⋯我去，換衣服」

「⋯⋯嗯嗯，是呢」

緹諾小跑到堆著的浴巾那裡拿出一條，然後脫掉鞋進入了屋內。我則是眯起眼睛，懷著恬靜淡然的心境看著嬉鬧的青梅竹馬他們。


§

「露～西～亞～，激流！沒有激流啊！這樣可無法修行！」

「露西亞醬～，桑拿！把桑拿變～出～來～！」

「吵死啦！啊～，真是的，吵死了！也不看看我在同時用著幾個魔法⋯⋯」

一個巨大的酒桶浮起，並在吵吵鬧鬧的盧克和莉茲的頭頂炸裂。然後黃金艾爾便猛地淋在兩人身上，與此同時，比之前更加濃烈的酒精也在四周蔓延開來。但即便如此也沒有混入料理和溫泉之中，不愧是露西亞。

莉茲捋起被酒打濕的頭髮，接著發出一聲悲鳴。

「幹什麼啊，好過分！」

「這個是⋯⋯醉劍嗎？是醉劍的修行對吧？」

被酒淋濕的肌膚閃過一抹光亮。看到渾身是酒也滿不在乎的哥哥大人和姐姐大人，脫掉浴衣圍上浴巾的緹諾戰戰兢兢地說道。

「Master⋯⋯這難不成就是，所謂的酒池肉林嗎？」

你是以哪層意思來使用的啊。

「嘛～，吃點料理怎麼樣？」

「這身打扮的話，我靜不下來，Master」

緹諾低頭看向自己的穿著，並抖了一下肩膀。緹諾的全身用長長的浴巾遮掩著。而且為了不讓其掉落，還緊緊地裹著進行固定。雖然總是穿著露肩的衣服所以在裸露程度上並沒有多大差別，但也許是因為蒸汽的緣故吧，她那雪白的肌膚正泛著紅暈。似乎是覺得非常丟臉。

「辛苦了，哥哥。之前那個，帶回來了嗎？」

「⋯⋯⋯⋯唔姆。放在了帝都」

在稍微有些遠的地方，和緹諾一樣裹著浴巾的西特莉正在用，打過肥皂的拖把給坐著的安塞姆擦洗背部。雖然看上去就像是在打掃寬大的牆壁一樣，但那無疑是名為擦背的行為。
西特莉滿臉都是笑容，看起來頗為開心。真是兄妹情深呢。因為背部太大，甚至還擺了許多凳子來踩著給他擦洗。

安塞姆的背部如同岩石一般巨大，肌肉也都是隆起的。而且表面還殘留的無數舊傷，那是時常站在前方獨自承受無數攻擊的證明。

安塞姆是隊伍的防御中樞，同時也是《嘆息的亡靈》的回復擔當。他的回復魔法強大到國家的大人物都會發來委託，然而所謂的回復魔法卻有著，唯獨對術者本人無效的惡劣性質。

雖然好像不會痛，但只要看到他的後背，托他的福沒有受過任何傷的我也會有些過意不去。

我站立起來，並拿著泳圈走近安塞姆。

「安塞姆，你辛苦了。我久違地，也來給你擦一下背吧」

「唔姆」

「可以嗎？啊，那麼，作為交換我就來擦克萊伊桑的背！可以吧，哥哥？」

「⋯⋯唔姆」

是要交換什麼啊。雖然不清楚，但我還是從西特莉那裡借來拖把，然後使勁地擦起安塞姆的背部。
儘管跟去探索時完全起不到任何作用，但與常人無異的牆壁清掃本領我自認是有的。發達隆起的背部確實相當難洗，我鼓足氣力擦拭起來。雖然是重體力勞動，但是累了的話只要去泡溫泉就行。
因為平時都沒有勞動過，所以這種事感覺也有些快樂。

當我正運用著拖把的時候，像鴨子一樣跟隨而來的緹諾怯生生地對我說道。

「Master⋯⋯那個⋯⋯請讓我，也來擦一擦。安塞姆哥哥，可以嗎？」

「⋯⋯唔姆」

雖然相識已經很久，但他一如既往地是個沉默寡言的男人。只不過因為眾多同伴的仰慕，他的聲音聽上去還是有些高興的。
當我正與緹諾進行交接的時候，注意到這邊的盧克和莉茲氣勢洶洶地並排跑了過來，緊接著爭奪起了拖把。

「啊～，很狡猾哦！修行嗎？是修行對吧！？也讓我來做一做！」

「哈啊？接下來輪到小緹，換句話說也就是輪到我！盧克醬去喝酒就行了吧！？來，給你！這個也是，修行！」

當莉茲打算把還沒喝空的酒杯塞給盧克的時候，盧克不耐煩地對她怒吼道。

「啊～！？怎麼可能有什麼喝酒的修行！你是在，把我當成笨蛋嗎！」

「哈啊！？克萊伊醬的話你聽，我的話你就不聽嗎？！」

「那是因為啊，莉茲！你的話裡──沒有信念！」

我的話裡也沒有喲⋯⋯大概從信念的多寡來說，我的話比莉茲的還要沒有分量喲。
不過這就是平時的光景。畢竟關係好才會吵架。

在紋絲不動的安塞姆身後怒目對罵的兩人。
將他們置於一旁，西特莉微笑著說道。只不過，她的情緒看上去也比平時要高亢一些。

「克萊伊桑，放著不管吧。比起這個⋯⋯我來給你擦背。畢竟我一次都沒有擦洗過」

「！！西特莉姐姐大人，太近──」

話音未落，就在西特莉要將胸部壓在我的背上之時，她的身體卻飛了出去。
隨後旋轉著一頭扎進溫泉裡，並激起一道狂亂的水花。

看到突然飛出去的姐姐大人，正在發出提醒的緹諾不由得張口結舌。單手拿著酒杯的露西亞目光呆滯地說道。

「西特莉，出局。真是的，一放鬆警惕馬上就這樣」

「露西亞姐姐大人⋯⋯剛才⋯⋯⋯⋯攻擊魔法」

「都是因為你們，我的魔法實力增長個不停！當我被周圍的人問，你是如何鍛鍊魔法的時候，你們知道我有多尷尬嗎！？」

順便一提，雖然露西亞比我強大，但是酒量卻有些差，因此攝取與莉茲她們相同的酒精時很快就會醉得神志不清。飲用魔力回復藥看來還會撒酒瘋。
不過話又說回來，吾妹即使喝醉也能正常地使用魔法呢。好強。好厲害。

看到一邊大口大口地啃著帶骨肉一邊進行監視的露西亞，緹諾不停顫抖著。

「Master！Master！剛才的，要是打中我的話⋯⋯我就死了」

「啊哈哈哈，沒事的喲，有掌握好分寸的」

而且不幹蠢事的話就不會被攻擊。因為我有警醒自己，所以至今都沒有被攻擊過。
扎進溫泉裡的西特莉顫顫巍巍地扒著浴池站了起來。由於浴巾被吹飛不見，微微泛紅的肌膚大面積裸露而出。

「⋯⋯真是的！明明我這麼努力了！竟然連一點獎勵都沒有──再說啊，只是給克萊伊桑擦背，為什麼需要露西亞的許可！？」

露西亞雙臂交叉而立，擋在發著牢騷的西特莉面前。精神顯得相當亢奮。

「那麼想黏在一起的話，要變成浴巾試試看嗎？雖然不知道能不能變回來」

「！？」

「誒咿！『西特莉，變成浴巾吧』！」

「呀～！！」

「變成浴巾吧！變成浴巾吧！啊，好煩，那種魔法，沒有！」

西特莉臉色大變地開始在溫泉裡游著逃跑。在將吃剩的骨頭化為灰燼之後，露西亞也脫掉浴衣跳進了溫泉裡。生成瀑布的云則是不停地打著雷。

對於姐姐大人們的狂亂之舉，緹諾顯得驚訝不已。

「西⋯⋯西特莉姐姐大人的悲鳴什麼的，我還是第一次聽到。此外，露西亞姐姐大人也⋯⋯」

畢竟露西亞和西特莉的，關係非常好呢。

「安塞姆，可以了嗎？」

「⋯⋯唔姆」

無論發生什麼都不為所動的安塞姆沖洗過身體，無視還在爭搶拖把的盧克與莉茲，然後慢慢地浸泡進了溫泉之中。從巨大的浴池裡溢出的熱水隨即流過我的腳邊。

在拿穩泳圈後，我也跟著跳入了溫泉。


§

委身於泳圈載沉載浮。緹諾給泳圈栓上細繩，並把繩頭綁在房柱之上。也不知道是有什麼含意。
我一邊仰望懸掛在夜空中的月亮，側耳傾聽同伴們的喧鬧，一邊享受著搖晃的感覺。

在身心放鬆的我的旁邊，一動不動地讓熱水浸到嘴邊的緹諾輕聲說道。

「我還是⋯⋯第一次見到，這麼熱鬧的場面」

盧克和莉茲正在溫泉裡比賽游泳。西特莉和露西亞似乎正在一邊泡溫泉，一邊比拼酒量。
而安塞姆則是閉著眼睛，在最深的地方靜靜地泡著熱水。雖然不知道在思考什麼，但他看上去很滿足。

這次說是度假，途中卻發生過形形色色的事。
雖然有許多倒霉的經歷，但結束後回想起來⋯⋯感覺上也是不錯的回憶。

雖然同時也給緹諾添了許多麻煩，但我希望這次的度假至少能讓她有所享受。
獵人是一個危險的職業。不過，開心的事也不是沒有。因為我毫無才能所以放棄了共同行動，但是像緹諾這般富有才能的話，肯定還會遇到更多開心的事吧。

「緹諾，度假⋯⋯你有得到享受嗎？」

「誒！？」

聽到突如其來的提問，緹諾睜大雙眼並沉默了好一會兒。
那雙純真的眼裡閃過各種各樣的色彩。也許是在回想這次的度假中發生的事吧。

我默默地等待著，最終緹諾的臉頰有些泛紅，在讓熱水浸到鼻尖後她微微點了點頭。

看來，美好的回憶也是有的呢。
太好了。結果好的話一切都好。剩下的就是去給伊娃她們買點特產，然後盡情地炫耀一番吧。

雖然帝都好像是出了大事，但要怪就怪邀請過卻不跟著來度假的他們自己。關於特產，也不知道溫泉龍饅頭如何。

泡過溫泉後，就出去喝酒。然後，放一放煙花又接著泡。屆時莉茲她們也會靜下來一些的吧。

當我正悠閑地想著這些開心事的時候，緹諾突然向我撞了過來。
隨後我的身體大幅一晃，腳下被緹諾光滑的大腿絆住。

我不由得說不出話來。換作莉茲的話我自然不會吃驚，但緹諾可是相當矜持且分得清TPO（時間地點場合）的類型。
不過，緹諾的臉上並沒有一絲羞澀。非要說的話，那是一副對現在的狀況感到難以置信的表情。

「⋯⋯⋯⋯怎麼了？」

「⋯⋯誒？不、不是的！Master，不知道為什麼，身體自己就漂了過來──」

就在此時，我注意到溫泉裡形成了一股洪流。
在寬敞的浴池的中心出現了一個巨大的漩渦，熱水正被吸入其中。也不知道是不是破了洞，水勢相當驚人。

注意到異常的盧克兩眼發亮，並踩著水試圖抵抗熱水的流動，然而在下一個瞬間還是被吞了下去。被盧克抓住手臂的莉茲在睜著眼睛的狀態下，也隨之消失得無影無踪。
露西亞和西特莉也差點被漩渦卷走，還好露西亞用魔法將她們固定在了原地。

「啊⋯⋯」

絆住的腿從腳下分離。緹諾輕聲叫了一聲之後，便被卷走消失在了漩渦之中。

熱水的體積迅速地減少，浴池的底部映入了眼簾。

位於漩渦中心的是，直徑一米左右的巨大坑洞。在放滿熱水之前毫無疑問是沒有的。

剩下的人則是只有，用游泳圈挺過來的我，以及露西亞和西特莉，還有因為體型而沒有被衝走的安塞姆。
伴隨著一陣響聲，數根長著似曾相識的黑色鈎爪的手指掛在了洞的邊緣之上。

面對一臉茫然的我，露西亞像是打了個冷顫似地說道。

「又、又來了是嗎，隊長」

「克萊伊桑，不是要一起放煙花的嗎！？」

雖然連西特莉都用責備的眼神看著我，但是這能怪我嗎？

「真是的！真是的！真是的！」

在使用魔法破壞洞口邊緣並將鈎爪剝離之後，露西亞便只身跳進了洞裡。
以現在的巨軀無法跳進洞裡的安塞姆也立即站立起來，然後踏出巨大的腳步聲去取寶具的全身鎧甲。

西特莉略顯為難地眨了眨眼睛。

「這個，要不要填起來？反正露西亞在的話也能回來⋯⋯」